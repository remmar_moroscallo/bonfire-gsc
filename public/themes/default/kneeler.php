<!-- .kneeler -->
    <div class="kneeler">

        <!-- .container -->
        <div class="container">

            <!-- .row -->
            <div class="row">

                <!-- .links-company -->
                <div class="col-lg-3 links-company column">
                    <h4>Company</h4>
                    <ul class="list-dotted">
                        <li><a href="<?php echo base_url().'gsc/page/about-us';?>">About Us</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/history';?>">History</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/our-customers';?>">Customers</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/partners';?>">Partners</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/careers';?>">Careers</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/contact-us';?>">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /.links-company -->

                <!-- .links-solutions -->
                <div class="col-lg-3 links-solutions column">
                    <h4>Solutions</h4>
                    <ul class="list-dotted">
                        <li><a href="http://hcm.gurango.com/" target="_blank">Gurango HCM</a></li>
                        <li><a href="http://xrm.gurango.com/" target="_blank">Gurango xRM</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/gurango-erp';?>">Gurango ERP</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/gurango-dms';?>">Gurango DMS</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/gurango-sms';?>">Gurango SMS</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/our-solutions';?>">View All »</a></li>
                    </ul>
                </div>
                <!-- /.links-solutions -->

                <!-- .links-resources -->
                <div class="col-lg-3 links-resources column">
                    <h4>Media</h4>
                    <ul class="list-dotted">
                        <li><a href="<?php echo base_url().'gsc/page/blog';?>">Blog</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/newsroom';?>">News</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/articles-and-resources#resources';?>">Resources</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/articles-and-resources#presskit';?>">Presskit</a></li>
                        <li><a href="<?php echo base_url().'gsc/page/articles-and-resources#brochure';?>">Brochure</a></li>
                        <li><a href="customer-login.html">Customer Login</a></li>
                    </ul>
                </div>
                <!-- /.links-resources -->

                <!-- .contact-info -->
                <div class="col-lg-3 contact-info column">
                    <a href="<?php echo base_url();?>"><img src="<?php echo $script_url;?>images/logo-gsc.png" class="img-responsive"></a>
                    <address>
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        G/F Topy’s Place Building<br />Industria corner Economia Streets<br />Quezon City Philippines 1110
                    </address>
                    <address>
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <a href="mailto:contact@gurango.com" style="color:black;">contact@gurango.com</a>
                    </address>
                </div>
                <!-- /.contact-info -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
<!-- /.kneeler -->