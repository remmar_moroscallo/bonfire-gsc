<?php

Assets::add_css(array('bootstrap.min.css','effects.css','component.css','responsive.css','stylesheet.css','mCustomScrollbar.css','jquery.flipster.min.css','bootstrap-select.css'));


Assets::add_js(array('jquery.min.js','bootstrap.min.js','bootstrap-select.js','modernizr.custom.js','classie.js','modalEffects.js','mCustomScrollbar.js','uiProgressButton.js','jquery.sticky-kit.min.js','scrollreveal.min.js','jquery.flipster.min.js','scripts.js'));

/*$inline  = '$(".dropdown-toggle").dropdown();';
$inline .= '$(".tooltips").tooltip();';
Assets::add_js($inline, 'inline');*/

?>
<!doctype html>
<head>
    <meta charset="utf-8">
    <title><?php
        echo isset($page_title) ? "{$page_title} : " : '';
        e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
    ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php e(isset($meta_description) ? $meta_description : ''); ?>">
    <meta name="author" content="<?php e(isset($meta_author) ? $meta_author : ''); ?>">
    <?php
    //Modernizr is loaded before CSS so CSS can utilize its features 
//    echo Assets::js('modernizr-2.5.3.js');
    ?>
    <?php echo Assets::css(); ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i"> 
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
</head>
<body>