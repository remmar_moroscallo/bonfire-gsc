        <!-- .block -->
        <div class="block-secondary block mailing-list">

            <!-- .container -->
            <div class="container width-1024">

                <!-- .row -->
                <div class="row">

                    <!-- -->
                    <div class="col-lg-12 align-center">
                        <h1 class="bottomless">Join Our Mailing List</h1>
                        <h4>Never miss out on the latest news and updates from GSC.</h4>                        
                        <form role="search">

                            <!-- -->
                            <div class="col-lg-9 search-email">
                                <input type="text" class="form-control full-width" placeholder="Email Address">
                            </div>

                            <!-- / -->                            

                            <!-- -->
                            <div class="col-lg-3 search-button">
                                <button type="button" class="btn btn-default full-width md-trigger" data-modal="modal-newsletter">Sign-Up Now</button>
                            </div>

                            <!-- / -->

                        </form>
                    </div>
                    <!-- / -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->

        </div>
        <!-- /.block -->