<!-- header -->
<header>
    <!--container-->
      <div class="container">
        <!-- .row -->
        <div class="row">

            <!-- -->
            <div class="col-lg-4 header-left">
                <a href="index.html"><img src="<?php echo $script_url;?>images/logo-gsc.png" class="img-responsive"></a>
            </div>
            <!-- / -->

                <!-- -->
                <div class="col-lg-8 header-right">
                    <!-- .top-links -->
                    <div class="top-links">
                        <ul>
                            <li><a href="<?php echo base_url().'gsc/page/careers';?>">Careers</a></li>
                            <li><a href="customer-login.html">Customer Login</a></li>
                        </ul>
                    </div>
                    <!-- /.top-links -->

                    <!-- .navbar -->
                    <nav class="navbar navbar-default">

                        <!-- .navbar-header -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- /.navbar-header -->

                        <!-- #navbar-collapse -->
                        <div class="collapse navbar-collapse" id="navbar-collapse">
            
                            <!-- .for-desktop -->
                            <ul class="nav navbar-nav navbar-right for-desktop">
                                <li><a href="#collapse-1" role="button" data-toggle="collapse">Our Solutions</a></li>
                                <li><a href="<?php echo base_url().'gsc/page/our-customers';?>">Our Customers</a></li>
                                <?php  $about_child2 = array('about-us','history','leadership','partners','careers','jobs','internships');?>
                                <li><a href="<?php echo base_url().'gsc/page/about-us';?>">About Us</a></li>
                                <?php  $article_resources = array('newsroom','blog','resources');?>
                                <li><a href="<?php echo base_url().'gsc/page/articles-and-resources';?>">Articles and Resources</a></li>
                            </ul>

                            <!-- .for-mobile -->
                            <ul class="nav navbar-nav navbar-right for-mobile">
                                <li><a href="#collapse-a" role="button" data-toggle="collapse">Our Solutions</a>

                                    <!-- #collapse-a -->
                                    <div class="collapse" id="collapse-a">

                                        <!-- .navdown -->
                                        <div class="navdown">

                                            <!-- -->
                                            <div class="col-lg-12">
                                                <ul>
                                                    <li><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-solutions'?>">Business Process Solutions</a></li>
                                                    <li><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-training'?>">Training & Assessment Services</a></li>
                                                    <li><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-hosting'?>">Hosting Services</a></li>
                                                </ul>
                                            </div>

                                            <!-- / -->

                                            <div class="clearfix"></div>
                                        </div>
                                        <!-- /.navdown -->

                                    </div>
                                    <!--/ #collapse-a -->

                                </li>

                                <li><a href="<?php echo base_url().'gsc/page/our-customers'?>">Our Customers</a></li>
                                <li><a href="<?php echo base_url().'gsc/page/about-us'?>">About Us</a></li>
                                <li><a href="<?php echo base_url().'gsc/page/articles-and-resources'?>">Articles and Resources</a></li>
                                <li><a href="<?php echo base_url().'gsc/page/careers'?>">Careers</a></li>
                                <li><a href="customer-login.html">Customer Login</a></li>
                            </ul>
                        </div>
                        <!-- /#navbar-collapse -->

                    </nav>
                    <!-- /.navbar -->

                </div>
                <!-- / -->

        </div>
        <!-- /.row -->
      </div>
    <!--/container-->

    <!-- .container -->
    <div class="container" id="navdown-holder">

        <!-- .row -->
        <div class="row">

            <!-- #collapse-1 -->
            <div class="collapse" id="collapse-1">

                <!-- .navdown -->
                <div class="navdown">

                    <!-- -->
                    <div class="col-lg-4 column">

                        <h4><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-solutions'?>">Business Process Solutions</a></h4>
                        <ul>
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-hcm-c.png" class="pull-left"></a>
                                <h5><a href="#">Gurango HCM</a></h5>
                                <p>Serving all your HR needs.</p>
                                <div class="clearfix"></div>
                            </li>
                            
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-xrm-c.png" class="pull-left"></a>
                                <h5><a href="#">Gurango xRM</a></h5>
                                <p>Managing your business relationships.</p>
                                <div class="clearfix"></div>
                            </li>
                            
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-erp-c.png" class="pull-left"></a>
                                <h5><a href="#">Gurango ERP</a></h5>
                                <p>Tracking what you have and where it’s going.</p>
                                <div class="clearfix"></div>
                            </li>
                            
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-dms-c.png" class="pull-left"></a>
                                <h5><a href="#">Gurango DMS</a></h5>
                                <p>Managing your data archives.</p>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-sms-c.png" class="pull-left"></a>
                                <h5><a href="#">Gurango SMS</a></h5>
                                <p>Connecting to your stakeholders on mobile.</p>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- -->
                    <div class="col-lg-4 column">
                        <h4><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-training'?>">Training & Assessment Services</a></h4>
                        <ul>
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-zendesk-c.png" class="pull-left"></a>
                                <h5><a href="#">Zendesk</a></h5>
                                <p>Helping you help your customers.</p>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-proxor-c.png" class="pull-left"></a>
                                <h5><a href="#">PROXOR</a></h5>
                                <p>Testing programmers the smart way.</p>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-mets-c.png" class="pull-left"></a>
                                <h5><a href="#">METS</a></h5>
                                <p>Teaching you how to Excel.</p>
                                <div class="clearfix"></div>
                            </li>

                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-gsa-c.png" class="pull-left"></a>
                                <h5><a href="#">GSA</a></h5>
                                <p>Teaching you the skills you need to succeed.</p>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- / -->

                    <!-- -->
                    <div class="col-lg-4 column">
                        <h4><a href="<?php echo base_url().'gsc/page/our-solutions#anchor-hosting'?>">Hosting Services</a></h4>
                        <ul>
                            <li>
                                <a href="#"><img src="<?php echo $script_url;?>images/logos/icon-ahs-c.png" class="pull-left"></a>
                                <h5><a href="#">AHS</a></h5>
                                <p>Providing total web hosting solutions.</p>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                        <br />
                        <a href="<?php echo base_url().'gsc/page/our-solutions';?>" class="btn btn-secondary">View All Solutions</a>

                    </div>
                    <!-- / -->

                    <div class="clearfix"></div>
                </div>
                <!-- /.navdown -->

            </div>
            <!--/ #collapse-1 -->

        </div>
        <!-- /.row -->
        
</div>
</header>
<!-- /header -->

