    <?php if ( ! isset($show) || $show == true) : ?>
    <hr />
    <!-- footer -->
        <footer>

            <!-- .container -->
            <div class="container">

                <!-- .row -->
                <div class="row">

                    <!-- -->
                    <div class="col-lg-8 copyright">
                        <p>
                            <a class="md-trigger" data-modal="modal-policy">Privacy Policy</a>
                            <span>•</span>
                            <a class="md-trigger" data-modal="modal-terms">Terms and Conditions</a>
                        </p>
                        <p>Copyright &copy; 2016 <strong>Gurango Software Corporation.</strong> All Rights Reserved.</p>
                    </div>
                    <!-- / -->                    

                    <!-- -->
                    <div class="col-lg-4">
                        <ul class="align-right social-media">
                            <li><a href="https://www.facebook.com/GurangoSoftware" target="_blank" class="fb">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/gurangosoftware" target="_blank" class="tw">
                            <i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="http://www.linkedin.com/company/gurango-software" target="_blank" class="in">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="http://www.youtube.com/gurangosoftware" target="_blank" class="yt">
                            <i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <!-- / -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->

        </footer>
        <!-- /footer -->        

        <!-- #slide-expander -->
            <a id="slide-expander" class="slide-expander fixed">
                <img src="<?php echo $script_url;?>img/btn-contact-text.png" class="for-desktop">
                <img src="<?php echo $script_url;?>img/btn-contact-icon.png" class="for-mobile">
            </a>
        <!-- /#slide-expander -->
        
        <!-- .slide-panel -->
        <div class="slide-panel">

            <!-- -->
            <div class="col-lg-4 align-center">
                <a href="#">
                    <i class="fa fa-comment" aria-hidden="true"></i>
                    <h4>Chat Now</h4>
                </a>
                <p class="bottomless">Talk to one of our representatives for real-time assistance.</p>
            </div>
            <!-- / -->
            
            <!-- -->
            <div class="col-lg-4 align-center">
                <a class="md-trigger md-setperspective" data-modal="modal-contact">
                    <i class="color-l-blue fa fa-phone" aria-hidden="true"></i>
                    <h4>Call Us</h4>
                </a>
                <p class="bottomless">Give us a ring and we'd be more than happy to assist you and answer your questions.</p>
            </div>
            <!-- / -->
            
            <!-- -->
            <div class="col-lg-4 align-center">
                <a href="<?php echo base_url().'gsc/page/contact-us';?>">
                    <i class="color-l-blue fa fa-envelope" aria-hidden="true"></i>
                    <h4>Get in Touch</h4>
                </a>
                <p class="bottomless">Send us a message about appointments, inquiries, feedback or comments.</p>
            </div>
            <!-- / -->
            
        </div>
        <!-- /.slide-panel -->

    <?php endif; ?>
	<div id="debug"><!-- Stores the Profiler Results --></div>
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php// echo js_path(); ?>jquery-1.7.2.min.js"><\/script>');</script>
-->
   </div><!--end of wrapper div-->
 <div class="md-overlay"></div>

    <?php echo Assets::js(); ?>
 <script>
		// Scroll Reveal
		window.sr = ScrollReveal({ reset: false });
		sr.reveal('.grid .paddless', { duration: 500 });
	</script>
</body>
</html>