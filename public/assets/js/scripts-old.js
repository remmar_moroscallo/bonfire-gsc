// Browse Button

$(function() {

	// We can attach the `fileselect` event to all file inputs on the page

	$(document).on('change', ':file', function() {

		var input = $(this),

			numFiles = input.get(0).files ? input.get(0).files.length : 1,

			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

		input.trigger('fileselect', [numFiles, label]);

	});



	// We can watch for our custom `fileselect` event like this

	$(document).ready( function() {

		$(':file').on('fileselect', function(event, numFiles, label) {

			var input = $(this).parents('.input-group').find(':text'),

				log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {

          		input.val(log);

			} else {

            	if( log ) alert(log);

			}

		});

	});

});



// ScrollBar

(function($){

	$(window).on("load",function(){

		$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default

		$.mCustomScrollbar.defaults.axis="yx"; //enable 2 axis scrollbars by default

		$(".md-scroll").mCustomScrollbar({theme:"minimal"});

	});

})(jQuery);



// Slide Panel

$(document).ready(function(){	

	$('#slide-expander').on('click',function(e){

		e.preventDefault();

		$('body').toggleClass('slide-expanded');

	});

	$('#slide-close').on('click',function(e){

		e.preventDefault();

		$('body').removeClass('slide-expanded');

	});

});



// Shrink On

$(function () {

	$(window).scroll(function () {

		var distanceY = $(this).scrollTop(),

			shrinkOn  = 300,

			header    = $("header");

			

			if (distanceY > shrinkOn) {

				header.addClass("shrink");

			} else {

			if (header.hasClass("shrink")) {

				header.removeClass("shrink");

			}

		}

	});  

})



// Smooth Scroll

$(function() {

	$('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {

		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

			var target = $(this.hash);

			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

			if (target.length) {

				$('html, body').animate({

				scrollTop: target.offset().top

			}, 600);

				return false;

			}

		}

	});

});



// Sticky Bar

$(".subnav").stick_in_parent();



// Reveal Sub Logo

jQuery(window).scroll(function() {

if (jQuery(this).scrollTop() > 100) {

    	//jQuery('.subnav-logo').stop().animate({ left: '25px' });

		jQuery('.subnav-logo').stop().fadeIn( "fast" );

	} else {

		//jQuery('.subnav-logo').stop().animate({ left: '-240px' });

		jQuery('.subnav-logo').stop().fadeOut( "fast" );

	}

});



// Flipster

//var coverflow = $("#coverflow").flipster();

var flat = $("#jobs_ribbon").flipster({

	style: 'flat',

	spacing: -0.25,

	loop: true,

	start: 'center',

	//autoplay: true,

	//fadeIn: 2000,

	//pauseOnHover: true,

	buttons: true

});

			

// Bootstrap Selector

$('.selectpicker').selectpicker({});



// Progress Button

[].slice.call( document.querySelectorAll( '.progress-button' ) ).forEach( function( bttn, pos ) {

	new UIProgressButton( bttn, {

		callback : function( instance ) {

		var progress = 0,

			interval = setInterval( function() {

				progress = Math.min( progress + Math.random() * 0.1, 1 );

				instance.setProgress( progress );



				if( progress === 1 ) {

					instance.stop( pos === 1 || pos === 3 ? -1 : 1 );

					clearInterval( interval );

				}

			}, 150 );

		}

	} );

} );



// Slider: Hsitory

$('#slider-history').carousel({

	interval: 3000,

	pause: false

})



// Slider: Partners

$('#slider-partners').carousel({

	interval: 30000,

	pause: false

})



// Panel Accordion

jQuery('.panel-heading a').click(function() {

    $('.panel-heading').removeClass('active');

    $(this).parents('.panel-heading').addClass('active');

});