<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gsc_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();

	}

		
	public function add_data($table_name,$data)
	{
		$this->db->insert($table_name,$data);
		$this->db->insert_id();
	}

	public function update_data($table_name,$table_column,$hash,$data)
	{
		$this->db->where($table_column,$hash);
		$this->db->update($table_name,$data);
		
		//$this->db->insert_id();
	}

	public function get_data($table_name,$table_column,$hash)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($table_column,$hash);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_one_data($table_name,$column_name,$id)
	{
		$this->db-> select('*');
		$this->db-> from($table_name);
		$this->db->where($column_name,$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}

   
  public function delete($table_name,$column_name,$n,$hash)
  {
  	for($i=0; $i < $n; $i++)
	{
	//$result = mysql_query("DELETE FROM member where member_id='$id[$i]'");
	$this->db->where($column_name, $hash[$i]);
	$this->db->delete($table_name); 
	}
  	
  }

      public function delete_data($hash,$table_column1,$table_name)
	{
		$this->db->where($table_column1, $hash);
		$this->db->delete($table_name);
	}

   public function update($table_name,$column_name,$n,$hash,$data)
  {
  	for($i=0; $i < $n; $i++)
	{
	//$result = mysql_query("DELETE FROM member where member_id='$id[$i]'");
	$this->db->where($column_name, $hash[$i]);
	$this->db->update($table_name,$data); 
	}
  	
 	 }

}