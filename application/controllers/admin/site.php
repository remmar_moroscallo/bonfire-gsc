<?php


if (!defined('BASEPATH')) exit('No direct script access allowed');


/**** Create by Authorname ******/


class Site extends Admin_Controller {


//--------------------------------------------------------------------


public function __construct()


{


parent::__construct();


Template::set('toolbar_title', 'Site');


$this->auth->restrict('Site.Site.View');


}


//--------------------------------------------------------------------


public function index()


{


Template::set_view('admin/site/index');


Template::render();


}


//--------------------------------------------------------------------


}
