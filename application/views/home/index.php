   <!-- .block -->
        <div class="block-primary block introduction">
            <a id="intro" name="intro"></a>            

            <!-- .container -->
            <div class="container width-1024">

                <!-- .row -->
                <div class="row">

                    <!-- -->
                    <div class="col-lg-5 pull-right column">
                        <img src="<?php echo $script_url;?>images/img-introduction.png" class="img-responsive">
                    </div>
                    <!-- / -->

                    <!-- -->
                    <div class="col-lg-7 align-right">
                        <h1>Smart Solutions,<br>Better Business.</h1>
                        <p>By combining sophisticated services with intuitive software built on the Microsoft technology platform, Gurango Software aims to improve your business processes through powerful and affordable solutions.</p>
                        <a href="<?php echo base_url().'gsc/page/about-us';?>" class="btn btn-default">Read More</a>
                    </div>
                    <!-- / -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->

        </div>
        <!-- /.block -->        

        <!-- .video-holder -->
        <div class="video-holder">

            <!-- .container -->
            <div class="container">

                <!-- .row -->
                <div class="row">
                    <a class="md-trigger" data-modal="modal-video"><img src="<?php echo $script_url;?>images/btn-play.png" class="btn-play ease"></a>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->

        </div>
        <!-- /.video-holder -->        

        <!-- .content -->
        <div class="content">

            <!-- .container -->
            <div class="container">

                <!-- .row -->
                <div class="row">

                    <!-- -->
                    <div class="col-lg-12 align-center">
                        <h1 class="color-l-blue">Recommended Reading</h1>
                    </div>
                    <!-- / -->                    

                    <!-- -->
                    <div class="col-lg-6 column">
                        <h3><a href="<?php echo base_url().'gsc/page/newsroom';?>">The Newsroom</a></h3>                        

                        <!-- .blog-item -->
                        <div class="blog-item">

                            <!-- .blog-img -->
                            <div class="col-lg-6 blog-img"><a href="#"><img src="<?php echo $script_url;?>images/img-1.jpg" class="img-responsive"></a></div>
                            <!-- /.blog-img -->                            

                            <!-- .blog-excerpt -->
                            <div class="col-lg-6 blog-excerpt">
                                <h4>
                                	<a href="#">Lorem ipsum dolor</a><br />
                                	<span>October 18 2016 | 5:17 PM</span>
                                </h4>
                                <p>Duis irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint...</p>
                                <a href="#" class="btn btn-default">Read More</a>
                            </div>
                            <!-- /.blog-excerpt -->                            

                            <div class="clearfix"></div>

                        </div>
                        <!-- /.blog-item -->                        

                        <!-- .blog-item -->
                        <div class="blog-item">

                            <!-- .blog-img -->
                            <div class="col-lg-6 blog-img"><a href="#"><img src="<?php echo $script_url;?>images/img-1.jpg" class="img-responsive"></a></div>
                            <!-- /.blog-img -->                            

                            <!-- .blog-excerpt -->
                            <div class="col-lg-6 blog-excerpt">
                                <h4>
                                	<a href="#">Lorem ipsum dolor</a><br />
                                	<span>October 18 2016 | 5:17 PM</span>
                                </h4>
                                <p>Duis irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint...</p>
                                <a href="#" class="btn btn-default">Read More</a>
                            </div>
                            <!-- /.blog-excerpt -->                            

                            <div class="clearfix"></div>

                        </div>
                        <!-- /.blog-item -->                        

                        <div class="clearfix"></div>

                    </div>
                    <!-- / -->                    

                    <!-- -->
                    <div class="col-lg-6 column">
                        <h3><a href="<?php echo base_url().'gsc/page/blog';?>">The GSC Blog</a></h3>                        

                        <!-- .blog-item -->
                        <div class="blog-item">

                            <!-- .blog-img -->
                            <div class="col-lg-6 blog-img"><a href="#"><img src="<?php echo $script_url;?>images/img-1.jpg" class="img-responsive"></a></div>
                            <!-- /.blog-img -->                          

                            <!-- .blog-excerpt -->
                            <div class="col-lg-6 blog-excerpt">
                                <h4>
                                	<a href="#">Lorem ipsum dolor</a><br />
                                	<span>October 18 2016 | 5:17 PM</span>
                                </h4>
                                <p>Duis irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint...</p>
                                <a href="#" class="btn btn-default">Read More</a>
                            </div>
                            <!-- /.blog-excerpt -->                            

                            <div class="clearfix"></div>

                        </div>
                        <!-- /.blog-item -->                        

                        <!-- .blog-item -->
                        <div class="blog-item">

                            <!-- .blog-img -->
                            <div class="col-lg-6 blog-img"><a href="#"><img src="<?php echo $script_url;?>images/img-1.jpg" class="img-responsive"></a></div>
                            <!-- /.blog-img -->                            

                            <!-- .blog-excerpt -->
                            <div class="col-lg-6 blog-excerpt">
                                <h4>
                                	<a href="#">Lorem ipsum dolor</a><br />
                                	<span>October 18 2016 | 5:17 PM</span>
                                </h4>
                                <p>Duis irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint...</p>
                                <a href="#" class="btn btn-default">Read More</a>
                            </div>
                            <!-- /.blog-excerpt -->                            

                            <div class="clearfix"></div>
                        
                        </div>
                        <!-- /.blog-item -->

                    </div>
                    <!-- / -->

                </div>
                <!-- /.row -->                

                <!-- -->
                <div class="row trusted-panel">

                    <!-- -->
                    <div class="col-lg-12 align-center">
                        <h1 class="color-l-blue">Trusted by 90+ Companies</h1>
                        <h4>Big or small, just starting out or long since established, your company is in safe hands with us. Here at GSC, we pride ourselves on making lasting relationships with all our satisfied customers.</h4>
                        <br />                    

                        <!-- .logo-holder -->
                        <table class="logo-holder">
                            <tr>
                                <td>
                                    <div class="logo-item">
                                        <div class="logo-details">
                                            <a href="<?php echo base_url().'gsc/page/story-smdc';?>"><img src="<?php echo $script_url;?>images/customers/logo-smdc.png">
                                            <br />Read the Story</a>
                                        </div>
                                        <div class="logo-overlay"></div>
                                        <img src="<?php echo $script_url;?>images/callout-img-1.jpg" class="img-responsive">
                                    </div>
                                </td>

                                <td rowspan="2">
                                    <div class="logo-item">
                                        <div class="logo-details">
                                            <a href="<?php echo base_url().'gsc/page/story-dl';?>"><img src="<?php echo $script_url;?>images/customers/logo-d-and-l.png">
                                            <br />Read the Story</a>
                                        </div>
                                        <div class="logo-overlay"></div>
                                        <img src="<?php echo $script_url;?>images/callout-img-3.jpg" class="img-responsive">
                                    </div>
                                </td>

                                <td>
                                    <div class="logo-item">
                                        <div class="logo-details">
                                            <a href="<?php echo base_url().'gsc/page/story-cebupac';?>"><img src="<?php echo $script_url;?>images/customers/logo-cebu-pacific.png">
                                            <br />Read the Story</a>
                                        </div>
                                        <div class="logo-overlay"></div>
                                        <img src="<?php echo $script_url;?>images/callout-img-2.jpg" class="img-responsive">
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="logo-item">
                                        <div class="logo-details">
                                            <a href="<?php echo base_url().'gsc/page/story-uef';?>"><img src="<?php echo $script_url;?>images/customers/logo-uef.png">
                                            <br />Read the Story</a>
                                        </div>
                                        <div class="logo-overlay"></div>
                                        <img src="<?php echo $script_url;?>images/callout-img-4.jpg" class="img-responsive">
                                    </div>
                                </td>

                                <td>
                                    <div class="logo-item">
                                        <div class="logo-details">
                                            <a href="<?php echo base_url().'gsc/page/story-mitsubishi';?>"><img src="<?php echo $script_url;?>images/customers/logo-mitsubishi.png">
                                            <br />Read the Story</a>
                                        </div>
                                        <div class="logo-overlay"></div>
                                        <img src="<?php echo $script_url;?>images/callout-img-5.jpg" class="img-responsive">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- /.logo-holder -->                        

                        <a href="<?php echo base_url().'gsc/page/our-customers';?>" class="btn btn-default">Learn More About Our Customers</a>

                    </div>
                    <!-- / -->

                </div>
                <!-- / -->

            </div>
            <!-- /.container -->

        </div>
        <!-- /.content -->        
